<?php

namespace App\Console\Commands;

use App\Models\Project;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ProjectClearedNotification;

class ClearProjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clear-projects';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all projects with an expiration date set to today';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        foreach (Project::all() as $project) {
            if ($project->expiration_date === date('Y-m-d')) {
                Project::destroy($project->id);
                try {
                    Notification::route('mattermost', config('moderation.notification_url'))
                        ->notify(new ProjectClearedNotification($project));
                } catch (\Exception $e) {
                    throw new \Exception("Impossible d’envoyer le rapport:" . $e->getMessage(), 500);
                }
            }
        }
    }
}
