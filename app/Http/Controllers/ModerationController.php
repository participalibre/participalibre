<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ReportProjectRequest;
use App\Notifications\NewReportNotification;
use Illuminate\Support\Facades\Notification;
use App\Http\Requests\ReportDestroyProjectRequest;
use App\Notifications\NewDestroyReportNotification;

class ModerationController extends Controller
{
    public function mark_nonfree(Request $request)
    {
        $data = $request->json()->all()["context"];

        if (config('moderation.token') == $data["token"]) {
            $project = Project::findOrFail($data["project_id"]);
            $project->is_free_software = false;
            $project->save();
            return response()->json([
                "ephemeral_text" => "Le projet a bien été marqué comme non libre"
            ]);
        } else {
            abort(403);
        }
    }
    public function mark_free(Request $request)
    {
        $data = $request->json()->all()["context"];

        if (config('moderation.token') == $data["token"]) {
            $project = Project::findOrFail($data["project_id"]);
            $project->is_free_software = true;
            $project->save();
            return response()->json([
                "ephemeral_text" => "Le projet a bien été marqué comme libre"
            ]);
        } else {
            abort(403);
        }
    }
    public function get_url(Request $request)
    {
        $data = $request->json()->all()["context"];

        if (config('moderation.token') == $data["token"]) {
            $project = Project::findOrFail($data["project_id"]);
            $url = "https://contribulle.org/projects/" . $data["project_id"] . "/edit/" . $project->admin_token;
            return response()->json([
                "ephemeral_text" => "L'url de modification/suppression est " . $url
            ]);
        } else {
            abort(403);
        }
    }
    public function send_report(ReportProjectRequest $request)
    {
        $report = $request->validated();

        try {
            Notification::route('mattermost', config('moderation.notification_url'))
                ->notify(new NewReportNotification($report));
        } catch (\Exception $e) {
            throw new \Exception("Impossible d'envoyer le signalement : " . $e->getMessage(), 500);
        }
    }

    public function send_destroy_report(ReportDestroyProjectRequest $request)
    {
        $report = $request->validated();
        $report['project'] = Project::findOrFail($report['project_id']);

        try {
            Notification::route('mattermost', config('moderation.notification_url'))
                ->notify(new NewDestroyReportNotification($report));
        } catch (\Exception $e) {
            throw new \Exception("Impossible d’envoyer le rapport du suppression :" . $e->getMessage(), 500);
        }
    }
    public function band(Request $request)
    {
        $data = $request->all();
        if (config('moderation.band_token') == $data["token"] && config('moderation.band_channel_id') == $data["channel_id"]) {
            Storage::delete('band.txt');
            Storage::put('band.txt', str_replace('!bandeau ', '', $data["text"]));
            return response()->json([
                "text" => "Bandeau mis à jour :tada:",
            ]);
        } else {
            abort(403);
        }
    }
}
