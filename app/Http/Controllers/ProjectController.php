<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Project;
use App\Models\Category;
use App\Notifications\CreateSuccessfully;
use App\Http\Requests\StoreProjectRequest;
use App\Notifications\NewProjectEditNotification;
use App\Notifications\NewProjectNotification;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        if (request()->input('tags')) {
            $projects = Project::with('tags')->whereHas('tags', function ($query) {
                $query->whereIn('slug', request()->input('tags'));
            })->where('disabled', false)->orderByDesc('created_at');
        } else {
            $projects = Project::where('disabled', false)->orderByDesc('created_at');
        }
        return view('projects.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'projects' => $projects->get(),
            'contributions' => json_decode(Storage::get('first-contribution.json'), true),
        ]);
    }

    public function projects(Request $request)
    {
        if ($request->input('tags')) {

            $projects = Project::with('tags')->whereHas('tags', function ($query) {
                $query->whereIn('slug', request()->input('tags'));
            })->where('disabled', false)->orderByDesc('created_at');
        } else {
            $projects = Project::where('disabled', false)->orderByDesc('created_at');
        }
        return view('projects.projects', [
            'projects' => $projects->get()
        ]);
    }

    public function create()
    {
        return view('projects.create', [
            'tags' => Tag::all(),
            'categories' => Category::all()
        ]);
    }

    public function store(StoreProjectRequest $request)
    {
        $validatedData = $request->validated();

        try {
            // Project is free software by default
            $validatedData['is_free_software'] = true;
            $project = Project::create($validatedData);
            // Generate a token to edit project
            $project->createAdminToken();
            $project->tags()->sync($validatedData['tags']);
            Notification::route('mattermost', config('moderation.notification_url'))
                ->notify(new NewProjectNotification($project));
            return redirect()->route('projects.success', $project)->with([
                'authorization' => 'success'
            ]);
        } catch (\Exception $e) {
            throw new \Exception("Impossible de créer le projet : " . $e->getMessage(), 500);
        }
    }

    public function show(Project $project)
    {
        if (!$project->disabled) {
            return view('projects.show', [
                'project' => $project,
            ]);
        } else {
            abort(404);
        }
    }

    public function edit(Project $project, string $token)
    {
        return view('projects.edit', [
            'project' => $project,
            'tags' => Tag::all(),
            'categories' => Category::all()
        ]);
    }

    public function update(StoreProjectRequest $request, Project $project, string $token)
    {
        $validatedData = $request->validated();
        try {
            $project->update($validatedData);

            $project->tags()->sync($validatedData['tags']);
            Notification::route('mattermost', config('moderation.notification_url'))
                ->notify(new NewProjectEditNotification($project));
            return redirect()->route('projects.edit.success', $project)->with([
                'authorization' => 'success'
            ]);
        } catch (\Exception $e) {
            throw new \Exception("Impossible de modifier le projet " . $e->getMessage(), 500);
        }
    }

    /**
     * Route after project was stored
     */
    public function success(Project $project)
    {
        abort_if(!session()->has('authorization'), 403);

        $project->notify(new CreateSuccessfully());

        return view('projects.success', [
            'link' => $project->path_admin,
            'project_id' => $project->id
        ]);
    }

    public function edit_success(Project $project)
    {
        abort_if(!session()->has('authorization'), 403);

        return view('projects.edit_success', [
            'project' => $project
        ]);
    }

    public function destroy(Project $project, string $token)
    {
        try {
            $project->delete();
            return redirect()->route('home')->with([
                'delete' => 'success'
            ]);
        } catch (\Exception $e) {
            throw new \Exception("Impossible de supprimer le projet " . $e->getMessage(), 500);
        }
    }
}
