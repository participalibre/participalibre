<?php

namespace App\Http\Controllers;

use App\Models\Project;

class RssController extends Controller
{
    public function feed()
    {
        return response()->view('feed', ['projects' => Project::where('disabled', false)])->header('Content-Type', 'application/xml');
    }

    public function feedTag($slug)
    {
        $project = Project::whereHas('tags', function ($q) use ($slug) {
            $q->where('slug', '=', $slug);
        })->where('disabled', false)->get();
        return response()->view('feed', ['projects' => $project])->header('Content-Type', 'application/xml');
    }
}
