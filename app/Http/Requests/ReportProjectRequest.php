<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'string', 'between:2,255'
            ],
            'email' => [
                'email', 'max:255'
            ],
            'description' => [
                'required', 'string', 'min:4'
            ],
            'project_id' => [
                'required', 'integer'
            ],
        ];
    }
}
