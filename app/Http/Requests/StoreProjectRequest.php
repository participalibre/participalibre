<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required', 'string', 'between:2,255'
            ],
            'description' => [
                'required', 'string', 'min:2'
            ],
            'url' => [
                'nullable', 'url', 'max:255'
            ],
            'email' => [
                'required', 'email', 'max:255'
            ],
            'creator' => [
                'required', 'string', 'between:2,255'
            ],
            'details' => [
                'required', 'string', 'min:2'
            ],
            'licence' => [
                'nullable', 'string', 'between:2,255'
            ],
            'remuneration' => [
                'nullable', 'string', 'between:2,255'
            ],

            'social_broadcast' => [
                'nullable', 'boolean'
            ],

            'tags' => [
                'required', 'array', 'min:1'
            ],

            'tags[*]' => [
                'required', 'numeric', 'exists:tags,id'
            ],
            'expiration_date' => [
                'nullable', 'date'
            ]
        ];
    }
}
