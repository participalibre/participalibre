<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use ThibaudDauce\Mattermost\Attachment;
use Illuminate\Notifications\Notification;
use ThibaudDauce\Mattermost\MattermostChannel;
use ThibaudDauce\Mattermost\Message as MattermostMessage;

class NewProjectEditNotification extends Notification
{
    use Queueable;

    protected $project;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($project)
    {
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [MattermostChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMattermost($notifiable)
    {
        return (new MattermostMessage)
            ->attachment(function (Attachment $attachment) {
                $attachment->fallback('Nouvelle modification d\' annonce')
                    ->success()
                    ->title('Nouvelle modification')
                    ->field('Nom de l\'annonce', $this->project->name)
                    ->field('Lien de l\'annonce', "https://contribulle.org/projects/".$this->project->id);
                });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {}
}
