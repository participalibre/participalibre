<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use ThibaudDauce\Mattermost\MattermostChannel;
use ThibaudDauce\Mattermost\Message as MattermostMessage;

class NewProjectNotification extends Notification
{
    use Queueable;

    protected $project;
    public function __construct($project)
    {
        $this->project = $project;
    }


    public function via($notifiable)
    {
        return [MattermostChannel::class];
    }


    public function toMattermost($notifiable)
    {
        $project = $this->project;
        return (new MattermostMessage)
            ->text("### Une nouvelle annonce est postée :tada: \n  #### " . $project['name'] . " \n Lien :right_anger_bubble: : ". config('app.url') ."/projects/" . $project['id'])
            ->attachment(function ($attachement) {
                $attachement->text("Modérer")
                    ->action([
                        'id' => 'edit',
                        'name' => 'Modifier/supprimer l\'annnonce',
                        'integration' => [
                            'url' => config('app.url').'/api/moderation/get_url',
                            'context' => [
                                'token' => config('moderation.token'),
                                'project_id' => $this->project->id
                            ],
                        ]
                    ])
                    ->action([
                        'id' => 'nonfree',
                        'name' => 'Marquer comme non libre',
                        'integration' => [
                            'url' => config('app.url').'/api/moderation/mark_nonfree',
                            'context' => [
                                'token' => config('moderation.token'),
                                'project_id' => $this->project->id
                            ],
                        ]
                    ])
                    ->action([
                        'id' => 'free',
                        'name' => 'Marquer comme libre',
                        'integration' => [
                            'url' => config('app.url').'/api/moderation/mark_free',
                            'context' => [
                                'token' => config('moderation.token'),
                                'project_id' => $this->project->id
                            ],
                        ]
                        ]);
            });
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'name' => $this->project->name,
            'id' => $this->project->id
        ];
    }
}
