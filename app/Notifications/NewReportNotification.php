<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use ThibaudDauce\Mattermost\Attachment;
use Illuminate\Notifications\Notification;
use ThibaudDauce\Mattermost\MattermostChannel;
use ThibaudDauce\Mattermost\Message as MattermostMessage;

class NewReportNotification extends Notification
{
    use Queueable;

    protected $report;
    public function __construct($report)
    {
        $this->report = $report;
    }


    public function via($notifiable)
    {
        return [MattermostChannel::class];
    }


    public function toMattermost($notifiable)
    {
        return (new MattermostMessage)
            ->attachment(function (Attachment $attachment) {
                $attachment->fallback('Nouveau Signalement')
                    ->success()
                    ->authorName($this->report["name"])
                    ->authorLink($this->report["email"])
                    ->title('Nouveau Signalement !')
                    ->field('Annonce concernée', '['. $this->report["project_id"] .']('. config('app.url') .'/projects/'. $this->report["project_id"] .')', true)
                    ->field('Description', $this->report["description"], false);
            });
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

    }
}
