<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use ThibaudDauce\Mattermost\MattermostChannel;
use ThibaudDauce\Mattermost\Message as MattermostMessage;

class ProjectClearedNotification extends Notification
{
    use Queueable;

    protected $project;
    public function __construct($project)
    {
        $this->project = $project;
    }


    public function via($notifiable)
    {
        return [MattermostChannel::class];
    }


    public function toMattermost($notifiable)
    {
        $project = $this->project;
        return (new MattermostMessage)
            ->text("### Projet supprimé automatiquement \n  #### " . $project['name'] . "(" . $project['id'] . ")");
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'name' => $this->project->name,
            'id' => $this->project->id
        ];
    }
}
