<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{

    private $max = 280;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('project-pres', project_presComponent::class);

        Blade::directive('truncate', function ($chaine) {
            return "<?= htmlspecialchars(strlen($chaine) > $this->max ? substr($chaine,0,$this->max).'…' : $chaine) ?>";
        });
    }
}
