<?php

namespace App\View\Components\Navbar;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\View\Component;

class Link extends Component
{

    /**
     * The active class
     * 
     * @var string
     */
    public $activeClass;


    /**
     * The url where the link is.
     * 
     * @var string 
     */
    public $to;

    /**
     * Boolean for say if the link is active.
     * 
     * @var boolean 
     */
    public $active;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($to, $activeClass = 'active-link')
    {
        $this->activeClass = $activeClass;
        $this->to = $to;
        $this->active = FacadesRequest::url() == $to;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.navbar.link');
    }
}
