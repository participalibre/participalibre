<?php

namespace App\View\Components\Utils;

use Illuminate\View\Component;

class Modal extends Component
{

    /**
     * The status of the modal.
     * 
     * @var string
     */
    public $status;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($status = "close")
    {
        $this->status = $status;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.utils.modal');
    }
}
