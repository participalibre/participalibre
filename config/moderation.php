<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Connection Name
    |--------------------------------------------------------------------------
    |
    | Laravel's queue API supports an assortment of back-ends via a single
    | API, giving you convenient access to each back-end using the same
    | syntax for every one. Here you may define a default connection.
    |
    */

    'token' => env('TOKEN'),
    'instance' => env('INSTANCE'),
    'notification_url' => env('NOTIFICATION_URL'),
    'band_token' => env('BAND_TOKEN'),
    'band_channel_id' => env('BAND_CHANNEL_ID')
];
