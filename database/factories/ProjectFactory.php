<?php

namespace Database\Factories;

use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'New Project',
            'description' => 'Project description',
            'url' => 'https://myprojects.org',
            'email' => 'john@example.com',
            'creator' => 'John',
            'details' => 'Beautiful Project',
            'licence' => 'GNU AGPLv3+',
            'expiration_date' => $this->faker->dateTimeBetween(),
            'remuneration' => 'Free',
            'is_free_software' => 1,
            'social_broadcast' => 0
        ];
    }

    public function withAdminToken()
    {
        return $this->state(function (array $attributes) {
            return [
                'admin_token' => Str::random(),
            ];
        });
    }
}
