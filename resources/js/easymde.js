import EasyMDE from "easymde";

document.querySelector("#description").removeAttribute('required');
document.querySelector("#details").removeAttribute('required');

// var easyMDE = new EasyMDE();

var description = new EasyMDE({
    element: document.getElementById("description"),
    autoDownloadFontAwesome: false,
    spellChecker: false,
    inputStyle: "textarea"
});

var details = new EasyMDE({
    element: document.getElementById("details"),
    autoDownloadFontAwesome: false,
    spellChecker: false,
    inputStyle: "textarea"
});
