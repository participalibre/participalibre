export class FormDestroy {
    constructor(formElement) {
        this.formElement = formElement;
        this.submit = document.querySelector("#modal-submit");
        this.submit.addEventListener("click", this.sendReport.bind(this));
        window.addEventListener("modal-open", () => {
            this.endLoading();
        });
    }

    startLoading() {
        this.submit.classList.add("loader");
        this.submit.classList.remove("arrow");
    }

    endLoading() {
        this.submit.classList.remove("loader");
        this.submit.classList.add("arrow");
    }

    async sendReport(event) {
        event.preventDefault();
        this.startLoading();
        try {
            const data = new FormData(this.formElement);
            console.log(data);
            const res = await fetch("/api/moderation/send_destroy_report/", {
                method: "POST",
                body: data,
            });
        } catch (error) {
            console.log(error);
        } finally {
            this.endLoading();
            window.location.replace(event.target.href);
        }
    }
}
