export class FormProject {
    /**
     * This class is use for check if all entries are fill before save
     * project.
     * @param {HTMLElement} formElement
     */
    constructor(formElement) {
        this.inputs = formElement.querySelectorAll("input");
        this.textarea = formElement.querySelectorAll("textarea");
        this.toggleElements = formElement.querySelectorAll(
            ".accordion > .toggle-element"
        );
        this.checkbox = formElement.querySelector(
            'input[type="checkbox"]#accept'
        );
        this.tagsSelected = [];

        formElement
            .querySelector('button[type="submit"]')
            .addEventListener("click", this.checkAllEntry.bind(this));

        formElement
            .querySelectorAll('input[type="checkbox"]:not(#accept)')
            .forEach((el) => {
                el.addEventListener(
                    "change",
                    this.computeTagsSelected.bind(this)
                );
            });

        this.toggleElements.forEach((toggleElement) => {
            toggleElement.addEventListener(
                "click",
                this.checkAccordion.bind(this)
            );
        });

        this.inputs.forEach((el) => {
            el.addEventListener("blur", (e) => this.checkEntry(e.target));
            el.addEventListener("input", (e) => this.checkEntry(e.target));
        });

        this.textarea.forEach((el) => {
            el.addEventListener("blur", (e) => this.checkEntry(e.target, true));
            el.addEventListener("input", (e) =>
                this.checkEntry(e.target, true)
            );
        });
    }

    /**
     * Method for check if an element is fill.
     * @param {HTMLElement} element element to check
     * @param {bool} force if force = true, the element has check even if it does not have required attributes
     * @returns bool, return true if an error has added.
     */
    checkEntry(element, force = false) {
        let addError = false;
        if ((force || element.required) && element.value === "") {
            this.addError(element);
            addError = true;
        } else {
            this.removeError(element);
        }
        return addError;
    }

    /**
     * Method for check if the accordion can be closes.
     * @param {Event} event event send when the user open or close an accordion
     */
    checkAccordion(event) {
        if (this.tagsSelected.length > 0) {
            this.addError(this.tagsSelected[0].parentNode.parentNode, true);
            event.stopImmediatePropagation();
        } else {
            this.removeAllAccordionError();
        }
    }

    checkAllAccordion() {
        let nbTagsSelected = 0;
        let nbCategoriesSelected = 0;
        let addError = false;

        this.toggleElements.forEach((toggleElement) => {
            let length =
                toggleElement.parentNode.querySelectorAll(
                    "input.cb:checked"
                ).length;

            console.log(toggleElement);

            if (length > 0) {
                nbTagsSelected += length;
                nbCategoriesSelected++;

                if (nbTagsSelected > 2) {
                    this.addError(toggleElement, true);
                    addError = true;
                }

                if (nbCategoriesSelected > 1) {
                    addError = true;
                    this.addError(toggleElement, true);
                }
            }
        });
        if (nbTagsSelected === 0) {
            this.addError(this.toggleElements[0], true);
            addError = true;
        }
        return addError;
    }

    /**
     * Method for check if the checkbox (#accept) is check.
     */
    checkCheckbox() {
        let addError = false;
        if (!this.checkbox.checked) {
            addError = true;
            this.addError(this.checkbox.parentNode.parentNode);
        }
        return addError;
    }

    /**
     * Method for compute the list of tags selected and add an error if more
     * of two tags was selected.
     * @param {Event} event event send when the user check a tag
     */
    computeTagsSelected(event) {
        if (event.target.checked) {
            if (this.tagsSelected.length >= 2) {
                event.target.checked = false;

                this.toggleElements.forEach((toggleElement) => {
                    if (
                        toggleElement.parentNode.querySelector("input:checked")
                    ) {
                        this.addError(toggleElement, true);
                    }
                });
            } else {
                this.tagsSelected.push(event.target);
            }
        } else {
            this.tagsSelected = this.tagsSelected.filter(
                (tag) => tag !== event.target
            );
            if (this.tagsSelected.length <= 2) {
                this.removeAllAccordionError();
            }
        }
    }

    /**
     * Method for add an error at an element.
     * @param {HTMLElement} element to add the error
     * @param {bool} accordion if true, the element is an accordion
     */
    addError(element, accordion = false) {
        if (element.parentNode.className.search("input-error") === -1) {
            element.parentNode.className += " input-error";

            if (accordion) {
                window.dispatchEvent(new Event("resize"));
            }
        }
    }

    /**
     * Method for remove the error of the element.
     * @param {HTMLElement} element to remove the error.
     * @param {bool} accordion if true, the element is an accordion.
     */
    removeError(element, accordion = false) {
        if (element.parentNode.className.search("input-error") !== -1) {
            element.parentNode.className =
                element.parentNode.className.split("input-error")[0];

            if (accordion) {
                window.dispatchEvent(new Event("resize"));
            }
        }
    }

    /**
     * Method for remove all error of the accordion.
     */
    removeAllAccordionError() {
        this.toggleElements.forEach((toggleElement) => {
            this.removeError(toggleElement, true);
        });
    }

    /**
     * Method for check all entry before send the form.
     * @param {Event} event
     */
    checkAllEntry(event) {
        this.inputs.forEach((el) => {
            if (this.checkEntry(el)) {
                event.preventDefault();
            }
        });
        this.textarea.forEach((el) => {
            if (this.checkEntry(el)) {
                event.preventDefault();
            }
        });

        if (this.checkAllAccordion() || this.checkCheckbox()) {
            event.preventDefault();
        }
    }
}
