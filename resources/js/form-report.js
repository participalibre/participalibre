export class FormReport {
    constructor(formElement) {
        this.element = formElement;
        this.description = document.querySelector("#report-description");
        this.creator = document.querySelector("#report-creator");
        this.email = document.querySelector("#report-email");
        this.submit = document.querySelector("#modal-submit");
        this.projectId = document.querySelector("#project-id").value;
        this.confirmElement = formElement.parentNode.querySelector(".confirm");

        this.description.addEventListener("blur", (_) =>
            this.checkEntry(this.description)
        );
        this.description.addEventListener("input", (_) =>
            this.checkEntry(this.description)
        );

        this.submit.addEventListener("click", this.sendReport.bind(this));
        window.addEventListener("modal-open", () => {
            this.endLoading();
        });
    }

    checkEntry(element) {
        let addErrror = false;
        if (element.value === "") {
            this.addError(element);
            addErrror = true;
        } else {
            this.removeError(element);
        }

        return addErrror;
    }

    checkAllEntry() {
        return this.checkEntry(this.description);
    }

    confirmForm() {
        dispatchEvent(new CustomEvent("confirm"));
    }

    addError(element) {
        if (element.parentNode.className.search("input-error") === -1) {
            element.parentNode.classList.add("input-error");
        }
    }

    removeError(element) {
        if (element.parentNode.className.search("input-error") !== -1) {
            element.parentNode.classList.remove("input-error");
        }
    }

    startLoading() {
        this.submit.classList.add("loader");
        this.submit.classList.remove("arrow");
    }

    endLoading() {
        this.submit.classList.remove("loader");
        this.submit.classList.add("arrow");
    }

    async sendReport(event) {
        this.startLoading();
        try {
            if (!this.checkAllEntry()) {
                const data = new FormData(this.element);
                const res = await fetch("/api/moderation/send_report/", {
                    method: "POST",
                    body: data,
                });
                this.confirmForm();
            }
        } catch (error) {
            console.log(error);
        } finally {
            this.endLoading();
        }
    }
}
