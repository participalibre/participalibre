export class Modal {
    constructor(
        element,
        openButton = undefined,
        confirmModal = undefined,
        option = {}
    ) {
        this.element = element;
        this.openButton = openButton;
        this.closeButtons = document.querySelectorAll(".modal-close");
        this.confirmModal = confirmModal;

        if (this.openButton) {
            this.openButton.addEventListener("click", this.open.bind(this));
        }

        if (this.closeButtons) {
            this.closeButtons.forEach((button) => {
                button.addEventListener("click", this.close.bind(this));
            });
        }

        window.addEventListener("keydown", (event) => {
            if (event.key === "Escape") {
                this.close(event);
            }
        });

        if (this.confirmModal) {
            window.addEventListener("confirm", () => {
                this.close();
                this.confirmModal.open();
            });
        }
    }

    open() {
        this.element.classList.add("modal--open");
        document.body.style.overflow = "hidden";
        dispatchEvent(
            new CustomEvent("modal-open", {
                detail: { modalId: this.element.id },
            })
        );
    }

    close() {
        this.element.classList.remove("modal--open");
        document.body.style.removeProperty("overflow");
        dispatchEvent(
            new CustomEvent("modal-close", {
                detail: { modalId: this.element.id },
            })
        );
    }
}
