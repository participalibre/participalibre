class NavbarMenu {
    /**
     * Class for create a burger menu.
     * @param {HTMLElement} toggleElement element to listen the click for display the MenuElement
     * @param {HTMLElement} menuElement element to display when the toggleElement was clicked
     */
    constructor(toggleElement, menuElement, option = {}) {
        this.toggleElement = toggleElement;
        this.menuElement = menuElement;
        this.breakpoint = option.breakpoint ? option.breakpoint : 960

        this.toggleElement.addEventListener(
            "click",
            this.displayMenu.bind(this)
        );

        window.addEventListener('resize', this.computeDisplaying.bind(this))
        document.addEventListener("click", this.hideMenu.bind(this));
        document.addEventListener("scroll", this.hideMenu.bind(this));
    }

    displayMenu() {
        this.menuElement.style.visibility = "visible";
        this.menuElement.style.opacity = 1;
    }

    hideMenu(event) {
        if (
            event.target !== this.toggleElement &&
            event.target !== this.menuElement &&
            window.innerWidth < this.breakpoint
        ) {
            this.menuElement.style.opacity = 0;
            this.menuElement.style.visibility = "hidden";
        }
    }

    computeDisplaying() {
        if (window.innerWidth < this.breakpoint) {
            this.menuElement.style.opacity = 0;
            this.menuElement.style.visibility = "hidden";
        } else {
            this.displayMenu();
        }
    }
}

new NavbarMenu(
    document.querySelector(".burger-menu"),
    document.querySelector(".links"),
    { breakpoint: 960 }
);
