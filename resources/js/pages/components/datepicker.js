import flatpickr from "flatpickr";
import 'flatpickr/dist/flatpickr.min.css'

export class DatePicker {
    /**
     *
     * @param element HTMLElement
     * @param options
     */
    constructor(element, options) {
        flatpickr(element, options)
    }
}
