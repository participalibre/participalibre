import {Accordions} from "../../accordion.js";
import {FormProject} from "../../form-project.js";
import {DatePicker} from "../components/datepicker";
import {French} from 'flatpickr/dist/l10n/fr'

const currentDate = new Date()
currentDate.setFullYear(currentDate.getFullYear() + 1)

new Accordions("accordion");
new FormProject(document.querySelector("#form-project"));
new DatePicker(document.querySelector("#publication-delay"), {
    minDate: "today",
    maxDate: currentDate,
    locale: French,
    dateFormat: "Y-m-d"
});
