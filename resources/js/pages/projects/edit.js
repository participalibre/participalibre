import { Accordions } from "../../accordion.js";
import { FormProject } from "../../form-project.js";
import { FormDestroy } from "../../form-destroy.js";
import { Modal } from "../../modal.js";

new Accordions("accordion");
new FormProject(document.querySelector("#form-project"));
new FormDestroy(document.querySelector("#form-destroy"));
new Modal(
    document.querySelector("#remove-modal"),
    document.querySelector("#remove-open")
);
