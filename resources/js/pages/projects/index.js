import { Accordions } from "../../accordion.js";
import { Slider } from "../../slider.js";
import { FilterMenu } from "../../filter-menu.js";
import { FiltersAjax } from "../../filters-ajax.js";

new Accordions("accordion");
new Slider(document.querySelector("#slider"), {
    slidesVisible: 4,
    slidesToScroll: 1,
});
new FilterMenu("#filters-form", "#filter-toggle-button");
new FiltersAjax(
    document.querySelectorAll(".buttons > .checkbox"),
    document.querySelectorAll(".filters-tags"),
    document.querySelector(".filter-loader"),
    document.querySelector("#index")
);
