import { Modal } from "../../modal.js";
import { FormReport } from "../../form-report.js";

const confirmModal = new Modal(document.querySelector("#report-confirm"));

new Modal(
    document.querySelector("#report-modal"),
    document.querySelector("#report-open"),
    confirmModal
);

new FormReport(document.querySelector("#form-report"));
