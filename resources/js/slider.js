export class Slider {
    /**
     * @callback moveCallback
     * @param {number} index
     */

    /**
     *
     * @param {HTMLElement}
     * @param {Object} options
     * @param {Number} [options.slidesToScroll=1] Nombre d’éléments à faire défilé
     * @param {Number} [options.slidesVisible=1] Nombre d’éléments visible  dans un slide
     * @param {Boolean} [options.loop=false] Défini si le slider doit boucler en fin de slide.
     */
    constructor(element, options = {}) {
        this.element = element;
        let children = [].slice.call(element.children);
        this.isMobile = true;
        this.isTablet = false;
        this.currentItem = 0;
        this.options = Object.assign(
            {},
            {
                slidesVisible: 1,
                slidesToScroll: 1,
                loop: false,
            },
            options
        );
        this.moveCallbacks = [];

        // Update DOM
        this.root = this.createDivWithClass("slider");
        this.container = this.createDivWithClass("slider-container");
        this.root.appendChild(this.container);
        this.element.appendChild(this.root);
        this.items = children.map((child) => {
            let item = this.createDivWithClass("slider-item");
            item.appendChild(child);
            this.container.appendChild(item);
            return item;
        });
        this.setStyle();
        this.createNavigation();

        // Events
        this.moveCallbacks.forEach((cd) => cd(0));
        this.onWindowResize();
        window.addEventListener("resize", this.onWindowResize.bind(this));
        this.root.addEventListener("keyup", (e) => {
            if (e.key === "ArrowRight") {
                this.next();
            } else if (e.key === "ArrowLeft") {
                this.prev();
            }
        });
    }

    /**
     * Applique les bonnes dimensions du carousel
     */
    setStyle() {
        let ratio = this.items.length / this.slidesVisible;
        this.container.style.width = ratio * 100 + "%";
        this.items.forEach((item) => {
            item.style.width = 100 / this.slidesVisible / ratio + "%";
        });
    }

    createNavigation() {
        const DISTANCE = 100;

        let nextButton = this.createDivWithClass("slider-next");
        let prevButton = this.createDivWithClass("slider-prev");
        let startX = 0;

        nextButton.appendChild(document.createElement("i"));
        prevButton.appendChild(document.createElement("i"));

        this.root.appendChild(nextButton);
        this.root.appendChild(prevButton);
        nextButton.addEventListener("click", this.next.bind(this));
        prevButton.addEventListener("click", this.prev.bind(this));
        this.root.addEventListener("touchstart", (e) => {
            // e.preventDefault();
            startX = e.changedTouches[0].pageX;
        });

        this.root.addEventListener("touchend", (e) => {
            let between = e.changedTouches[0].pageX - startX;
            if (between > DISTANCE) {
                this.prev();
            } else if (-between > DISTANCE) {
                this.next();
            }
        });

        if (this.options.loop) {
            return;
        }

        this.onMove((index) => {
            if (index === 0) {
                prevButton.classList.add("slider-prev-hidden");
            } else {
                prevButton.classList.remove("slider-prev-hidden");
            }

            if (
                index >= this.items.length ||
                this.items[this.currentItem + this.slidesVisible] === undefined
            ) {
                nextButton.classList.add("slider-next-hidden");
            } else {
                nextButton.classList.remove("slider-next-hidden");
            }
        });
    }

    next() {
        this.goToItem(this.currentItem + this.slidesToScroll);
    }

    prev() {
        this.goToItem(this.currentItem - this.slidesToScroll);
    }

    /**
     * Déplace le slider vers l’élément ciblé.
     * @param {Number} index
     */
    goToItem(index) {
        if (index < 0) {
            if (this.options.loop) {
                index = this.items.length - this.slidesVisible;
            } else {
                return;
            }
        } else if (
            index >= this.items.length ||
            (this.items[this.currentItem + this.slidesVisible] === undefined &&
                index > this.currentItem)
        ) {
            if (this.options.loop) {
                index = 0;
            } else {
                return;
            }
        }
        let translateX = (index * -100) / this.items.length;
        this.container.style.transform = `translate3d(${translateX}%, 0, 0)`;
        this.currentItem = index;
        this.moveCallbacks.forEach((cd) => cd(index));
    }

    /**
     *
     * @param {moveCallback} cd
     */
    onMove(cd) {
        this.moveCallbacks.push(cd);
    }

    onWindowResize() {
        const mobile = window.innerWidth < 960;
        const tablet = window.innerWidth < 1200 && window.innerWidth >= 960;

        if (tablet !== this.isTablet) {
            this.isTablet = tablet;
            this.setStyle();
            this.moveCallbacks.forEach((cd) => cd(this.currentItem));
        }

        if (mobile !== this.isMobile) {
            this.isMobile = mobile;
            this.setStyle();
            this.moveCallbacks.forEach((cd) => cd(this.currentItem));
        }
    }

    /**
     *
     * @param {string} className
     * @returns {HTMLElement}
     */
    createDivWithClass(className) {
        let div = document.createElement("div");
        div.setAttribute("class", className);
        return div;
    }

    /**
     * @returns {Number}
     */
    get slidesToScroll() {
        return this.isTablet
            ? 2
            : this.isMobile
            ? 1
            : this.options.slidesToScroll;
    }

    /**
     * @returns {Number}
     */
    get slidesVisible() {
        return this.isMobile
            ? 1
            : this.isTablet
            ? 2
            : this.options.slidesVisible;
    }
}
