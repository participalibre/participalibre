@extends('layouts.app')

@section('title', 'À propos')

@section('content')
<section class="text-secondary text-justify text-large" id="about">
  <article class="width-3-4">
    <h1>Qu’est-ce que Contribulle ?</h1>
    <h2 class="text-normal">« Je veux contribuer à des projets libres mais je ne sais pas comment et où. »</h2>
    <p>
      Nous sommes souvent confronté·es à cette question ! <br />
      C'est pourquoi Contribulle souhaite y répondre.
    </p>
    <p>
      Cette plateforme - sous licence - libre veut simplifier le contact entre des projets qui manquent de compétences,
      et de super·be·s contributeur·rice·s qui pourraient leur donner un coup de main.

    </p>

    <h2 class="text-normal">Tout projet partageant les valeurs du <a
        href="https://fr.wikipedia.org/wiki/Logiciel_libre">logiciel libre</a> et <a
        href="https://fr.wikipedia.org/wiki/Communs">des communs</a> est le bienvenu.</h2>
    <p>
      <em>Le logiciel libre et les communs, c’est une affaire qui nous concerne tout·e·s.</em>
    </p>

    <p>Pour assurer l'émancipation et la liberté, à la fois individuelle et collective, la coopération et le partage de
      connaissances sont des principes plus qu’essentiels.
      Contribulle s’inscrit dans un mouvement qui met en avant la contribution par et pour tout le monde, pour
      réellement participer et construire une société inclusive.</p>

    <p>
      <em>Chacun·e peut aider à sa manière !</em>
    </p>

    <h2 class="text-normal">Derrière Contribulle : une équipe motivée et une démarche progressive</h2>
    <div>
      <p>
        Contribulle est créée de façon itérative, avec une équipe composée de personnes aux compétences variées ! Pour
        papoter avec nous, vous pouvez :
      </p>
      <ul>
        <li>Nous rejoindre sur <a href="https://framateam.org/ux-framatrucs/channels/contribulle"
            target="_blank">Framateam
          </a>
        </li>
        <li>Nous écrire par mail sur <a
            href="mailto:bonjour[arobase]contribulle.org">bonjour[arobase]contribulle.org</a></li>
      </ul>
      <p>
        Nous avons hâte de contribuer au libre et aux communs avec vous !
      </p>

      <h2>Le collectif Contribulle est composé de :</h2>
      <ul>
        <li>Méli (@icimeli), UX/UI design</li>
        <li><a href="https://www.maiwann.net" target="_blank" rel="noopener noreferrer">Maiwann</a></li>
        <li><a href="http://lelibreauquotidien.fr" target="_blank" rel="noopener noreferrer">Llaq</a> Développeur
          backend et
          frontend</li>
        <li>Dav.c, UX Design</li>
        <li><a href="http://fedabian.fr" target="_blank" rel="noopener noreferrer">Da-max</a> Développeur frontend</li>
      </ul>

      <h2 class="text-normal">Merci Framasoft !</h2>

      <p>Le projet Contribulle est né dans le cadre d'un <a href="https://contribateliers.org/" target="_blank"
          rel="noopener noreferrer">Contribatelier</a> au milieu de l'année 2020. Ces évènements, plébiscités par <a
          href="https://framasoft.org/" target="_blank" rel="noopener noreferrer">Framasoft</a>, visent à réunir des
        personnes souhaitant s'impliquer dans la culture du Libre quelles que soient leurs compétences techniques.</p>

      <p>L'équipe Contribulle tient donc à remercier Framasoft pour avoir contribué à la naissance de ce projet ainsi
        que pour son soutien technique et financier (hébergement, nom de domaine).</p>

      <h2 class="text-normal">Mentions légales</h2>

      <p>Ce site est édité par le collectif Contribulle et hébergé chez Gandi : 63-65 boulevard Masséna, 75013 Paris</p>
    </div>
  </article>
  <div class="text-center margin-large-x">
    <a href="/" class="link">Retour à l’accueil</a>
  </div>
</section>
@endsection