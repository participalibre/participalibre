<div class="project">
    <button type="button" class="button-outline">{{ $category }}</button>
    <i class="delimiter"></i>
    <button type="button" class="button-radius button-outline">{{ $subcategory }}</button>
    <i class="delimiter"></i>
    <p class="label">{{ $title }}</p>
    <p class="sublabel">{{ $description }}</p>
    <button type="button" class="button-outline button-radius">En savoir plus</button>
</div>
