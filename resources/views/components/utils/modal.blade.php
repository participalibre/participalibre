<dialog aria-hidden="true" {{ $attributes->merge(['class' => 'modal modal--' . $status]) }}>
    <article class="modal__content">
        <header>
            {{ $header }}
        </header>
        <section>
            {{ $section ?? '' }}
        </section>
        <footer>
            {{ $footer ?? '' }}
        </footer>
    </article>
</dialog>
<div class="modal-filter"></div>