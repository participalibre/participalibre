@extends('layouts.app')

@section('title', "Page de référence des composants")

@section('content')
    <div class="demo">
        <h1>Composants</h1>
        <x-project_pres category="Hello" subcategory="Heyo"  title="Meetic du libre" description="dublabla"/>
        <h2>Inputs</h2>
        <label for="test-input-text" class="label">Un label de premier niveau</label>
        <label for="test-input-text" class="sublabel">Un label de second niveau</label>
        <input type="text" name="test-input-text" placeholder="Input type text">
        <input type="text" class="is-combo" name="test-input-text-combo" placeholder="Input type text + classe is-combo">
        <button type="button" class="button-primary" name="button">button-primary</button>
        <button type="button" class="button-outline" name="button">button-outline</button>
        <button type="button" class="button-outline button-radius" name="button">button-outline button-radius</button>
        <button type="button" class="button-secondary" name="button">button-secondary</button>
        <a class="link" href="#">link</a>
        <a class="link is-small" href="#">link is-small</a>
        <a class="link is-medium" href="#">link is-medium</a>
        <a class="link is-big" href="#">link is-big</a>
        <label>
          input checkbox avec checkmark
        <input type="checkbox" name="" value="">
          <span class="checkmark"></span>
        </label>
        <textarea name="textarea" placeholder="Textarea"></textarea>
<button type="button" class="button-circle">+</button>
    </div>
@endsection
