<?=
'<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL
?>
<rss version="2.0">
    <channel>
        <title>
            <![CDATA[{{ config('app.name') }}]]>
        </title>
        <link>
        <![CDATA[{{ url()->full() }}]]>
        </link>
        <description>
            <![CDATA[La contribution pour tout le monde !]]>
        </description>
        <language>fr</language>
        <pubDate>{{ now() }}</pubDate>

        @foreach($projects as $project)
        <item>
            <title>
                <![CDATA[{{ $project->name }}]]>
            </title>
            <link>{{ url("/projects/{$project->id}") }}</link>
            <description>
                <![CDATA[{{ $project->description }}]]>
            </description>
            @foreach ($project->tags as $tag)
            <category>
                {{ $tag->name }}
            </category>
            @endforeach
            <author>
                <![CDATA[{{ $project->creator  }}]]>
            </author>
            <guid>{{ $project->id }}</guid>
            <pubDate>{{ $project->created_at->toRssString() }}</pubDate>
        </item>
        @endforeach
    </channel>
</rss>
