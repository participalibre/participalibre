@extends('layouts.app')

@section('title', 'Demande de contribution')

@section('style')
    @vite(['resources/css/easymde.css'])
@endsection

@section('content')
    <section class="add-form">
        <h1>Ma demande de contribution</h1>
        <h2>
            Une demande de contribution correspond à un type de besoin.
            Remplissez autant de demandes que vous avez de besoins !<br>
            Si vous ne savez pas trop par où commencer, vous pouvez retrouver nos astuces pour <a
                href="{{ route('tips-contribution') }}" target="_blank">accueillir des
                contributeur·ices</a>.
        </h2>
        <form method="post" action="/projects" class="child-margin-vertical-medium" id="form-project">
            @csrf
            <div>
                <label for="name" class="label">Le nom de votre projet</label>
                <label for="name" class="sublabel">Afin d’identifier rapidement votre projet</label>
                <input placeholder="Contribulle" type="text" name="name" id="name" required maxlength="255">
                <p class="message-error">Ce champs est requis</p>
            </div>
            <div>
                <label for="description" class="label">Sa présentation</label>
                <label for="description" class="sublabel">Description du service proposé</label>
                <textarea
                    placeholder="Facilitateur d’échanges entre porteur·ses de projets et contributeur·rices en ligne"
                    name="description" id="description" cols="30" rows="10" required></textarea>
                <p class="message-error">Ce champs est requis</p>
            </div>
            <div>
                <label for="url" class="label">Son lien</label>
                <label for="url" class="sublabel">
                    Son adresse URL, si disponible.<br/>
                    Attention, le lien doit commencer par http(s)://
                </label>
                <input placeholder="Lien" type="url" name="url" id="url" maxlength="255">
            </div>
            <div>
                <label for="licence" class="label">Sa licence</label>
                <label for="licence" class="sublabel">Son nom ou lien si disponible</label>
                <input placeholder="AGPL v3, Creative Commons..." type="text" name="licence" id="licence"
                       maxlength="255">
            </div>
            <div>
                <label class="label">Le type de contribution souhaitée</label>
                <label class="sublabel">Choisissez ce dont vous avez besoin dans une seule catégorie</label>
                <div class="child-margin-vertical-small" accordions-hide-all>
                    @foreach ($categories as $category)
                        <div class="choose accordion">
                            <div class="toggle-element columns flex-space-between">
                                <div class="column margin-small-left">
                                    <p class="label">
                                        {{ $category->name }}
                                    </p>
                                    <p class="message-error">Privilégiez <span class="text-bold">2 besoins</span>
                                        dans <span class="text-bold">une seule catégorie</span> pour ne pas perdre les
                                        contributeur·rices
                                    </p>
                                </div>
                                <i class="chevron-up chevron"></i>

                            </div>
                            <i class="delimiter"></i>
                            <div class="content">
                                <div class="buttons columns flex-left padding-small">
                                    @foreach ($category->tags as $tag)
                                        <input type="checkbox" name='tags[]' id="tag_{{ $tag->id }}" class="cb"
                                               value="{{ $tag->id }}">
                                        <label for="tag_{{ $tag->id }}">
                                            {{ $tag->name }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div>
                <label for="details" class="label">Détails de la contribution</label>
                <label for="details" class="sublabel">Précisez le besoin</label>
                <textarea placeholder="Je ne peux pas traduire mes textes" name="details" id="details" cols="30"
                          rows="10"
                          required></textarea>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <label for="remuneration" class="label">Rémunération</label>
                <label for="remuneration" class="sublabel">Précisez si la contribution sera rémunérée</label>
                <input maxlength="255" placeholder="Financière ou autre" type="text" name="remuneration"
                       id="remuneration">
            </div>
            <div>
                <label for="publication-delay" class="label">Date d'expiration de votre demande</label>
                <label for="publication-delay" class="sublabel">Définir une date de suppression de votre annonce afin d'actualiser la liste des demandes de contribution pour les contributeur·rices</label>
                <div>
                    <input type="text"
                           placeholder="Durée de publication de votre demande"
                           name="expiration_date" id="publication-delay">
                </div>
            </div>
            <div>
                <label for="creator" class="label">Votre prénom / pseudo / structure</label>
                <label for="creator" class="sublabel">Pour vous reconnaître</label>
                <input maxlength="255" placeholder="Zoé (Agence Kitrouvtout)" type="text" name="creator" id="creator"
                       required>
                <p class="message-error">Ce champs est requis</p>
            </div>
            <div>
                <label for="email" class="label">Votre adresse mail</label>
                <label for="email" class="sublabel">Pour vous contacter. Cette info sera affichée sur la page de votre
                    projet</label>
                <input placeholder="bonjour@contribulle.org" type="email" name="email" id="email" required
                       maxlength="255">
                <p class="message-error">Ce champs est requis</p>
            </div>
            <div>
                <div class="columns">
                    <div class="margin-small-right">
                        <input type="checkbox" value="1" name="social_broadcast" id="social_broadcast"
                               class="checkbox"/>
                        <label for="social_broadcast"></label>
                    </div>
                    <div>
                        <label for="social_broadcast" class="label">J’accepte que ma demande de contribution soit
                            diffusée sur les réseaux sociaux (liens vers
                            <a href="https://mastodon.tedomum.net/web/@contribulle/"> le profil Mastodon</a>
                            et vers <a href="https://twitter.com/contribulle"> le profil Twitter</a> de
                            Contribulle)</label>
                    </div>
                </div>
            </div>
            <div>
                <div class="columns">
                    <div class="margin-small-right">
                        <input type="checkbox" name="accept" class="checkbox" id="accept" required>
                        <label for="accept"></label>
                    </div>
                    <div>
                        <label for="accept" class="label">J'accepte que ces informations soient publiées sur la
                            plateforme</label>
                        <label for="accept" class="sublabel">Vous pourrez modifier ou retirer ce formulaire</label>
                    </div>
                </div>
                <p class="message-error margin-none">Pour consentir, la case doit être cochée</p>
            </div>
            <div class="btns">
                <a href="javascript:history.back()" style="margin-right: 40px;" class="link">Retour</a>
                <button type="submit" class="button-primary arrow">Valider</button>
            </div>
        </form>
    </section>
@endsection

@section('script')
    @vite(['resources/js/pages/projects/create.js', 'resources/js/easymde.js'])
@endsection
