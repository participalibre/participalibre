@extends('layouts.app')

@section('title', 'Editer un projet')

@section('style')
    @vite(['resources/css/easymde.css'])
@endsection

@section('content')
    <section class="add-form">
        <h1>Votre demande de contribution</h1>
        <h2>
            Vous pouvez éditer ou supprimer votre demande de contribution.
        </h2>
        <form method="post" action="/projects/{{ $project->id }}/{{ $project->admin_token }}" id="form-project"
              class="child-margin-vertical-medium">
            @csrf
            @method('PATCH')
            <div>
                <label for="name" class="label">Le nom de votre projet</label>
                <label for="name" class="sublabel">Afin d’identifier rapidement votre projet</label>
                <input placeholder="Participalibre" value="{{ $project->name }}" type="text" name="name"
                       id="name" required>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <label for="description" class="label">Sa présentation</label>
                <label for="description" class="sublabel">Description du service proposé</label>
                <textarea
                    placeholder="Facilitateur d’échanges entre porteur·ses de projets et contributeur·rices en ligne"
                    name="description" id="description" cols="30" rows="10"
                    required>{{ $project->description }}</textarea>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <label for="url" class="label">Son lien</label>
                <label for="url" class="sublabel">Son adresse URL, si c'est possible</label>
                <input placeholder="Lien" type="url" value="{{ $project->url }}" name="url" id="url" required>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <label for="licence" class="label">Sa licence</label>
                <label for="licence" class="sublabel">Son nom ou lien si c’est possible </label>
                <input value="{{ $project->licence }}" placeholder="GNU AGPL v3, Creative Commons..." type="text"
                       name="licence" id="licence" required>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <p class="label">Le type de contribution souhaitée</p>
                <p class="sublabel">Choisissez ce dont vous avez besoin dans une seule catégorie (2 compétences
                    maximum)</p>
                <div class="child-margin-vertical-small">
                    @foreach ($categories as $category)
                        <div class="choose accordion">
                            <div class="toggle-element columns flex-space-between">
                                <div class="column margin-small-left">
                                    <p class="label">
                                        {{ $category->name }}
                                    </p>
                                    <p class="message-error">Privilégiez <span class="text-bold">2 besoins</span>
                                        dans <span class="text-bold">une seule catégorie</span> pour ne pas perdre les
                                        contributeur·rices
                                    </p>
                                </div>
                                <i class="chevron-up chevron"></i>
                            </div>
                            <i class="delimiter"></i>
                            <div class="content">
                                <div class="buttons columns flex-left padding-small">
                                    @foreach ($category->tags as $tag)
                                        <input type="checkbox" name='tags[]' id="tag_{{ $tag->id }}" class="cb"
                                               value="{{ $tag->id }}"
                                               @foreach ($project->tags as $exists_tags)
                                                   @if ($exists_tags->id == $tag->id)
                                                       checked
                                            @endif @endforeach>
                                        <label for="tag_{{ $tag->id }}">
                                            {{ $tag->name }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div>
                <label for="details" class="label">Détails de la contribution</label>
                <label for="details" class="sublabel">Précisez le besoin</label>
                <textarea placeholder="Je ne peux pas traduire mes textes" name="details" id="details" cols="30"
                          rows="10"
                          required>{{ $project->details }}</textarea>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <label for="remuneration" class="label">Rémunération</label>
                <label for="remuneration" class="sublabel">Précisez si la contribution sera rémunérée</label>
                <input value="{{ $project->remuneration }}" placeholder="Financière ou autre" type="text"
                       name="remuneration" id="remuneration" required>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <label for="creator" class="label">Votre prénom / pseudo / structure</label>
                <label for="creator" class="sublabel">Pour vous reconnaître</label>
                <input value="{{ $project->creator }}" placeholder="Zoé (Agence Kitrouvtout)" type="text" name="creator"
                       id="creator" required>
            </div>
            <div>
                <label for="email" class="label">Votre adresse mail</label>
                <label for="email" class="sublabel">Pour vous contacter. Cette info sera affichée sur la page de votre
                    projet</label>
                <input value="{{ $project->email }}" placeholder="contact@participalibre.fr" type="email"
                       name="email" id="email" required>
                <p class="message-error">Ce champs est requis</p>

            </div>
            <div>
                <p class="label margin-none">
                    Suppression
                </p>
                <p class="sublabel margin-none">
                    Pour supprimer définitivement votre annonce, c’est ici que ça se passe !
                </p>
                <div class="margin-medium-top">
                    <button class="button-danger" type="button" id="remove-open">Supprimer
                        la demande de contribution
                    </button>
                </div>
            </div>
            <div class="btns">
                <a href="javascript:history.back()" style="margin-right: 40px;" class="link">Retour</a>
                <button type="submit" class="button-primary arrow">Valider</button>
            </div>
        </form>
    </section>

    <x-utils.modal id="remove-modal">
        <x-slot name="header">
            <h1>Supprimer mon annonce</h1>
        </x-slot>
        <x-slot name="section">
            <p class="text-bold">Pourquoi souhaitez-vous supprimer votre annonce ?</p>
            <p>
                Si vous avez trouvé un·e contributeur·rice par l’intermédiaire de Contribulle, votre retour d’expérience
                nous intéresse : comment se sont passés la contribution, l’échange avec le·a contributeur·rice ?
                Avez-vous
                des suggestions pour améliorer les contributions aux projets libres et/ou notre plateforme ?
            </p>
            <form action="#" id="form-destroy">
                <textarea name="description" id="remove-description" cols="10" rows="2"
                          placeholder="Nous avons trouvé une contributrice qui a pu traduire nos textes, tout s’est bien passé ! Nous avons créé un canal de discussion dédié pour faciliter les échanges. Elle nous a bien aidé dans cette tâche c’est pourquoi nous voulons supprimer l’annonce."></textarea>
                <input type="hidden" id="project-id" name="project_id" value="{{ $project->id }}">
            </form>
        </x-slot>
        <x-slot name="footer">
            <a class="link arrow modal-close">Revenir à l’annonce</a>
            <a id="modal-submit" href="/projects/{{ $project->id }}/{{ $project->admin_token }}"
               class="button-primary arrow margin-small-left">Supprimer
                définitivement</a>
        </x-slot>
    </x-utils.modal>
@endsection

@section('script')
    @vite(['resources/js/pages/projects/edit.js', 'resources/js/easymde.js'])
@endsection
