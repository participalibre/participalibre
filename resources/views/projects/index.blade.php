@extends('layouts.app')

@section('title', 'Les projets qui ont besoin d’aide')

@section('content-nav')
    <form method="get" action="/projects" id="filters-form" class="opened">
        <div class="filters-header">
            <h2>
                Filtrer les contributions
            </h2>
            <div class="filters-tags">
            </div>
            <div class="filters-button" id="filters-button">
                <input type="submit" value="Filtrer les projets">
            </div>
        </div>
        <div class="filters-content">
            @foreach ($categories as $category)
                <div class="filters accordion">
                    <div class="toggle-element filter">
                        <div class="margin-small-left">
                            <p class="label">
                                {{ $category->name }}
                            </p>
                        </div>
                        <i class="chevron-up chevron"></i>
                    </div>
                    <i class="delimiter"></i>

                    <div class="content">
                        <div class="buttons columns flex-left padding-small">
                            @foreach ($category->tags as $tag)
                                <input type="checkbox" name='tags[]' id="tag_{{ $tag->id }}"
                                       class="checkbox is-mini filter-checkbox" value="{{ $tag->slug }}">
                                <label for="tag_{{ $tag->id }}">
                                    {{ $tag->name }}
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="filters-footer filters-tags">
        </div>
        <div id="filter-toggle-button">
            <i class="filter-close"></i>
        </div>
    </form>
@endsection

@section('content-header')
    <div class="padding-medium background-accent overflow-hidden">
        <h1>C’est votre première contribution et vous êtes perdu⋅e⋅s ?
            <br>
            On vous a sélectionné quelques projets
        </h1>
        <div id="slider">
            @foreach ($contributions as $contribution)
                <a href="{{ $contribution['link'] }}" class="slider-content text-decoration-none" target="_blank">
                    <div class="card-header">
                        <h2 class="text-normal">{{ $contribution['name'] }}</h2>
                    </div>
                    <div class="card-body">
                        <p>
                            {{ $contribution['description'] }}
                        </p>
                    </div>
                    <div class="inverse card-footer text-right">
                        <i class="arrow-right"></i>
                        <span>Voir le site</span>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection

@section('content')
    <div class="filter-loader" style="display: none"></div>
    <section id="index">
        <h1>Les projets qui ont besoin d’un coup de main</h1>
        <div class="child-margin-vertical-medium">
            @foreach ($projects as $project)
                <x-project.item :project="$project"/>
            @endforeach
        </div>
    </section>
@endsection
@section('script')
    @vite(['resources/js/pages/projects/index.js'])
@endsection
