@extends('layouts.app')

@section('title', $project->name)

@section('content-header')
    @if (!$project->is_free_software)
        <div class="inverse">
            <p class="text-normal text-center padding-small margin-none">
                <i class="icon-danger"></i> Ce projet n’est pas libre, mais l’équipe de modération a estimé qu’il avait
                tout de même
                sa place sur Contribulle.
            </p>
        </div>
    @endif
@endsection

@section('content')
    <section id="show">
        <header>
            <div class="columns flex-wrap">
                <div class="category text-large">{{ $project->tags()->first()->category->name }}</div>
                @foreach ($project->tags as $tag)
                    <div class="tag text-large">{{ $tag->name }}</div>
                @endforeach
            </div>
            <h1>{{ $project->name }}</h1>
            <p>Publiée le {{ $project->created_at->format('d/m/Y à H:i') }}</p>
        </header>
        <section class="child-margin-vertical-medium width-1-2">
            <div>
                <h2 class="text-normal margin-none">Description</h2>
                <p class="margin-none">@parsedown($project->description)</p>
            </div>

            @if ($project->url)
                <div>
                    <h2 class="text-normal margin-none">Lien</h2>
                    <a href="{{ $project->url }}" target="_blank" class="text-normal text-primary">{{ $project->url }}</a>
                </div>
            @endif

            @if ($project->licence)
                <div>
                    <h2 class="text-normal margin-none">Licence</h2>
                    <p class="margin-none text-normal text-secondary">{{ $project->licence }}</p>
                </div>
            @endif

            <div>
                <h2 class="text-normal margin-none">Détails de la contribution souhaitée</h2>
                <div class="margin-none">@parsedown($project->details)</div>
            </div>

            @if ($project->remuneration)
                <div>
                    <h2 class="text-normal margin-none">Rémunération</h2>
                    <p class="margin-none">{{ $project->remuneration }}</p>
                </div>
            @endif

            <div>
                <h2 class="text-normal margin-none">Contact</h2>
                <p class="margin-none">{{ $project->creator }} : <a class="text-primary"
                                                                    href="mailto:{{ $project->email }}">{{ $project->email }}</a>
                </p>
            </div>
        </section>
        <div class="project-actions">
            <div class="width-1-2">
                <button class="button-danger icon-danger" id="report-open">Signaler l’annonce</button>
            </div>
            <div class="width-1-2">
                <a href="/" class="link">Retour à l’accueil</a>
            </div>
        </div>
    </section>
    <x-utils.modal id="report-modal">
        <x-slot name="header">
            <h1>Signaler l’annonce</h1>
        </x-slot>
        <x-slot name="section">
            <form action="#" id="form-report" class="default">
                <div>
                    <label class="label" for="report-description">Pourquoi cette annonce n’est pas appropriée sur
                        Contribulle ?</label>
                    <textarea name="description" id="report-description" cols="10" rows="2"
                              placeholder="Le projet n’est pas libre ? La demande ne correspond pas aux besoins sélectionnés ?"></textarea>
                    <p class="message-error">Ce champs est requis</p>
                </div>
                <div>
                    <label for="report-creator" class="label">Votre prénom / pseudo / structure</label>
                    <label for="report-creator" class="sublabel">Facultatif</label>
                    <input maxlength="255" placeholder="Zoé (Agence Kitrouvtout)" type="text" name="name"
                           id="report-creator" required>
                </div>
                <div>
                    <label for="report-email" class="label">Votre adresse mail</label>
                    <label for="report-email" class="sublabel">Pour vous contacter (facultatif)</label>
                    <input placeholder="bonjour@contribulle.org" type="email" name="email" id="report-email" required
                           maxlength="255">
                    <input type="hidden" id="project-id" name="project_id" value="{{ $project->id }}">
                </div>
            </form>
        </x-slot>
        <x-slot name="footer">
            <a class="link modal-close" href="#">Retour</a>
            <button type="submit" class="button-primary loader arrow" id="modal-submit">Envoyer</button>
        </x-slot>
    </x-utils.modal>

    <x-utils.modal id="report-confirm">
        <x-slot name="header">
            <h1>Signaler l’annonce</h1>
        </x-slot>
        <x-slot name="section">
            <p class="text-bold text-center">
                Votre signalement a été envoyé à l’équipe de modération de Contribulle,
                merci de votre aide !
            </p>
            <p class="text-center">
                Les modérateur·ice·s analyseront la ou les raisons que vous avez reportée(s)
                et agiront en conséquence.
            </p>
        </x-slot>
        <x-slot name="footer">
            <section class="columns flex-column flex-item-center">
                <a class="button-primary arrow modal-close">Revenir à l’annonce</a>
                <a href="/projects" class="button-primary arrow modal-close margin-small-top">Revenir à la page
                    « Projets »</a>
            </section>
        </x-slot>
    </x-utils.modal>
@endsection

@section('script')
    @vite(['resources/js/pages/projects/show.js'])
@endsection
