@extends('layouts.app')

@section('title', 'Conseils pour accueillir une nouvelle personne contributrice')

@section('content')
<section id="tips-contribution" class="text-secondary text-justify text-large">
  <article class="width-3-4">
    <h1>Comment accueillir une nouvelle personne contributrice ?</h1>
    <div>
      <p class="text-italic">
        Cette liste d'astuces est là pour donner des idées aux porteur·euses de projets qui veulent accueillir de
        nouvelles
        personnes. Nous ne sommes pas là pour donner des leçons, et si quelque chose ne te convient pas ou ne te parle
        pas,
        c'est que ça n'est pas adapté, aucun souci pour ça !
      </p>

      <p>
        Arriver dans un nouveau projet, c'est assez impressionnant pour pas mal de personnes. Il y a le projet, les
        contributeurices déjà présent·es, les façons de fonctionner… et il faut s'acclimater à tout ça ! C'est pas
        facile
        !
      </p>

      <p>
        Pour que tout se passe bien et vous préparer à accueillir cette nouvelle personne dans les meilleures conditions
        possibles, vous pourriez :
        <ol>
          <li>
            Choisir le contact d'une personne à qui s'adresser
          </li>

          <li>
            Mettre en place un outil facile d'accès pour échanger (messagerie instantanée type Mattermost, liste de
            diffusion)
            qui n'est pas une forge logicielle
          </li>
          <li>
            Lister les types de contributions attendues

            <ol>
              <li>
                en faisant attention de bien expliquer le problème que vous
                rencontrez et pour lequel vous avez besoin d'un coup de main
              </li>
              <li>
                et en mettant en avant, sur votre site si vous en avez un, cette liste
              </li>
            </ol>
          </li>
          <li>
            Prédéfinir qui est disponible, au sein du collectif, pour accueillir les nouvelles personnes. Iel pourra
            être le marraine / parraine qui se chargera de l'accueil, de l'explication du fonctionnement existant, et
            pourra répondre aux questions !
          </li>
        </ol>
      </p>
    </div>
  </article>
</section>
@endsection