@extends('layouts.app')

@section('title', 'Bienvenue')

@section('content')

@if (session('delete'))
<div class="inverse border-radius">
    <p class="icon-danger text-center margin-none padding-small">
        Votre projet a bien été supprimé
    </p>
</div>
@endif
<section class="home">
    <h1 class="home-title">Construisons ensemble <br class="visible@s" /> des projets plus conviviaux <br /> et plus
        respectueux <br class="visible@s" /> de nos libertés</h1>
    <div class="home-buttons">
        <div class="column">
            <a href="{{ route('projects.create') }}" class="tile">
                <h2>J'ai un projet</h2>
                <span class="desc">
                    Et j'aimerais avoir un coup de main
                </span>
            </a>
        </div>
        <div class="column">
            <a href="{{ route('projects.index') }}" class="tile">
                <h2>Je veux contribuer</h2>
                <span class="desc">
                    J'ai envie de participer à l'évolution d'un outil
                </span>
            </a>
        </div>
    </div>
</section>
@endsection