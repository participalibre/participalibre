<?php

use App\Http\Controllers\ModerationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/projects', [ProjectController::class, 'projects'])->name('projects.projects');
Route::prefix("moderation")->controller(ModerationController::class)->name('moderation.')->group(function () {
    Route::post('get_url',  'get_url')->name('get_url');
    Route::post('mark_nonfree',  'mark_nonfree')->name('mark_nonfree');
    Route::post('mark_free',  'mark_free')->name('mark_free');
    Route::post('send_report',  'send_report')->name('send_report');
    Route::post('send_destroy_report',  'send_destroy_report')->name('destroy_report');
    Route::post('band', 'band')->name('band');
});
