import {defineConfig} from "vite";
import laravel from "laravel-vite-plugin";

export default defineConfig({
    plugins: laravel({
        input: [
            'resources/sass/app.scss',
            'resources/css/easymde.css',
            'resources/js/easymde.js',
            'resources/js/app.js',
            'resources/js/pages/projects/show.js',
            'resources/js/pages/projects/index.js',
            'resources/js/pages/projects/edit.js',
            'resources/js/pages/projects/create.js'
        ],
        refresh: true
    })
})
